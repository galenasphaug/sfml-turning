# turning makefile
EXE = turning

CC=g++ # C++ compiler
CC_PARAMS=-Wall -pedantic -std=c++11 # Compiler flags
SFML_LIBS=-lsfml-system -lsfml-window -lsfml-graphics # Include SFML libraries

$(EXE): turning.cpp
	$(CC) $(CC_PARAMS) $^ -o $@ $(SFML_LIBS)

run: $(EXE)
	./$(EXE)
install: $(EXE)
	sudo cp $(EXE) /usr/local/bin/
uninstall:
	sudo rm /usr/local/bin/$(EXE)
clean:
	rm -f $(EXE) *.o *~ .*~
