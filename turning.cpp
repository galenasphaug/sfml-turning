#include <iostream>
#include <cmath>
#include <queue>
#include <SFML/Graphics.hpp>

const sf::Vector2f PLAYER_SIZE(20.0, 30.0);
const float PLAYER_SPEED = 5.0; // Pixels to move per frame
const float PLAYER_TURN_DEGREES = PLAYER_SPEED; //10.0; // How many degrees to turn the player per frame
const double DEG_TO_RAD = M_PI / 180.0;

// Move the player the direction they are facing (getRotation)
void movePlayer(sf::Shape * player, double distance) {
  if (!player) return;
  double radians = player->getRotation() * DEG_TO_RAD;
  player->move(distance * sin(radians),
	       -distance * cos(radians));
}

int main() {
  sf::VideoMode view(600, 400);
  sf::RenderWindow window(view, "", sf::Style::Close | sf::Style::Resize);

  sf::RectangleShape player(PLAYER_SIZE);

  sf::RectangleShape pathShape(sf::Vector2f(1.f, 1.f));
  std::vector<sf::Vector2f> path;
  
  // Color the player and put them in the top right
  player.setFillColor(sf::Color::White);
  player.setOrigin(PLAYER_SIZE / 2.f);
  player.setPosition(PLAYER_SIZE / 2.f);

  window.setVerticalSyncEnabled(false);
  window.setFramerateLimit(60);

  double speed = 0.f;
  //double angularMomentum = 0;
  
  while (window.isOpen()) {

    sf::Event e;
    while (window.pollEvent(e)) {
      switch (e.type) {
      case sf::Event::Closed:
	window.close();
	break;
      case sf::Event::MouseButtonPressed: //Move player to mouse position when clicked
	player.setPosition(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
	break;
      default:
	break;
      }
    } // event

    // Quit
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q)) window.close();

    // Rotate the player
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) player.rotate(PLAYER_TURN_DEGREES);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)) player.rotate(-PLAYER_TURN_DEGREES);

    // Find where the player will travel to and move them
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W)) {
      //speed = 1.f;
      if (speed <= 0.9f) speed += 0.1f;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) {
      //speed = -1.f;
      if (speed >= -0.9f) speed -= 0.1f;
    }

    //std::cout << speed << std::endl;

    movePlayer(&player, PLAYER_SPEED * speed);

    if (speed > 0.1 || speed < -0.1) speed /= 1.1f;
    else speed = 0;
    
    // Draw the player
    window.clear();
    window.draw(player);

    // Footprints
    path.push_back(player.getPosition());

    for (unsigned i = 0; i < path.size(); i++) {
      pathShape.setPosition(path[i]);
      window.draw(pathShape);
    }
    
    if (path.size() > 500) path.erase(path.begin(), path.begin() + 1);
    
    window.display();
  }
  return EXIT_SUCCESS;
}
