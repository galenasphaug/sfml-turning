# sfml-turning
Go around in circles and leave a trail and stuff. `WASD` controls, `Q` to quit.

## Compiling

	make
	./turning

OR

	make run

## Installing
(installs to `~/.local/bin`)

	make install
	turning
